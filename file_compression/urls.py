from django.urls import path
from .views import CompressView

urlpatterns = [
    path('compress', CompressView.as_view())
]
