from threading import Thread
from zipfile import ZipFile, ZIP_DEFLATED
import pika, os

def task_compress(file_object):
    path_zip = file_object.file.path + ".zip"

    with ZipFile(path_zip, "w") as zip:
        zip.write(
            file_object.file.path,
            compress_type=ZIP_DEFLATED
        )

def task_message(thread_compress, file_object, settings):
    path_zip = file_object.file.path + ".zip"
    path_file = file_object.file.path
    code = file_object.code
    current_percentage = 0.1

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            settings.RABBITMQ_URL,
            virtual_host=settings.VHOST,
            credentials=(
                pika.PlainCredentials(
                    settings.USERNAME,
                    settings.PASSWORD
                )
            )
        )
    )

    channel = connection.channel()

    channel.exchange_declare(
        exchange=settings.NPM,
        exchange_type='direct'
    )

    while thread_compress.is_alive():
        try:
            zip_percentage = os.path.getsize(path_zip) / os.path.getsize(path_file)

            if zip_percentage > current_percentage:
                channel.basic_publish(
                    exchange=settings.NPM,
                    routing_key=code,
                    body=str(int(current_percentage * 100)) + "%",
                )

                current_percentage = float("{:.1f}".format(zip_percentage)) + 0.1
        except OSError:
            pass

    channel.confirm_delivery()

    message_not_sent = True

    while message_not_sent:
        try:
            channel.basic_publish(
                exchange=settings.NPM,
                routing_key=code,
                body="100%",
                mandatory=True
            )

            message_not_sent = False
        except pika.exceptions.UnroutableError:
            pass

    connection.close()

def async_compress(file_object, settings):
    thread_compress = Thread(
        target=task_compress,
        args=(file_object,)
    )
    thread_compress.start()

    thread_message = Thread(
        target=task_message,
        args=(thread_compress, file_object, settings)
    )
    thread_message.start()
