from django.conf import settings

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST

from .async_task import async_compress
from .serializers import FileSerializer

# Create your views here.
class CompressView(APIView):
    def post(self, request):
        data = request.data

        if request.META.get("HTTP_X_ROUTING_KEY"):
            data["code"] = request.META.get("HTTP_X_ROUTING_KEY")

        serializer = FileSerializer(data=data)

        if serializer.is_valid():
            file_object = serializer.save()

            async_compress(
                file_object,
                settings
            )

            return Response(serializer.data, status=HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
