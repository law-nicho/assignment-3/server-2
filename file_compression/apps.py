from django.apps import AppConfig


class FileCompressionConfig(AppConfig):
    name = 'file_compression'
