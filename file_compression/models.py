from django.db import models

# Create your models here.
class File(models.Model):
    file = models.FileField()
    code = models.CharField(max_length=7)
